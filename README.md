# TG-ChatGPT

## Description
This is a simple Telegram bot that uses the OpenAI GPT-3 language model to generate responses to user inputs. The bot can be started using the /start command, and it can be used by sending messages to the bot with the prefix input:. For example, to ask the bot to generate a response to the message "What is the weather like today?", the user should send the message input: What is the weather like today? to the bot.

The bot uses the python-telegram-bot library to communicate with Telegram, and it uses the OpenAI API to generate responses to user inputs. The chat history for each user is stored in a separate file in the users_chat_history directory.

The bot also provides several other commands, including /help to display a list of available commands, and /delete to delete the user's chat history.

## Getting Started
To use this bot, you will need to create a Telegram bot and obtain an OpenAI API key. Then, you should update the config.ini file with your bot key and API key.

To run the bot, you can simply run the main() function in the mybot.py file.

## Requirements
This bot depends on the following Python libraries:
- Python 3.6+
- python-telegram-bot
- openai


## Usage
1. Clone the repository and navigate to the directory.
2. Install the required Python modules:

```
pip install -r requirements.txt
```

3. Rename config_sample.ini to config.ini and fill in the required information
```ini
[SYSTEM]
BOT_KEY=<your_bot_token_here>
WHITELIST=<authorized_user_ids_here>
OPENAI_API_KEY=<your_openai_api_key_here>
```
4. Start the bot:
```
python mybot.py
```

## Commands
- `/start`: Start the bot and show a welcome message.<br>
- `/help`: Show the available commands.<br>
- `/delete`: Delete the chat history of the user.<br>

