import os
import openai
import asyncio
import logging
import telegram
import configparser
from telegram import Update
from telegram.ext import Application, CommandHandler, ContextTypes, MessageHandler, filters
from telegram.constants import ParseMode


logging.basicConfig(
    filename='TG_bot.log',
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.ERROR
)
logger = logging.getLogger(__name__)

if not os.path.exists('users_chat_history'):
    os.mkdir('users_chat_history')


def read_config(sections, key, path='config.ini'):
    config = configparser.ConfigParser()
    config.read(path)
    return config[sections][key]


# async def send_message(message, chat_id):
#     bot = telegram.Bot(token=read_config('SYSTEM', 'BOT_KEY'))
#     await bot.send_message(text=message, chat_id=chat_id, parse_mode=ParseMode.MARKDOWN)


async def echo(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    WHITELIST = read_config('SYSTEM', 'WHITELIST').replace(' ', '').split(',')
    if str(update.message.from_user.id) not in WHITELIST:
        message = "{} 您好\n您無權限可以進行此操作喔!\n請將自己的chatID加入白名單\n您的ChatID為{}".format(update.message.from_user.username, update.message.from_user.id)
        await update.message.reply_text(message, parse_mode=ParseMode.MARKDOWN)
    elif update.message.text.startswith('input '):
        chat_history = ''
        # 建立 該玩家聊天歷史紀錄
        if os.path.exists('users_chat_history/{}_history.txt'.format(update.message.from_user.id)):
            with open('users_chat_history/{}_history.txt'.format(update.message.from_user.id), 'r') as f:
                chat_history = f.read()
        input = update.message.text[6:]
        chat_history += 'Human:{}\n'.format(input)
        await update.message.reply_text('機器人回應生成中.... \n 您的輸入是: {}'.format(input), parse_mode=ParseMode.MARKDOWN)
        openai.api_key = read_config('SYSTEM', 'OPENAI_API_KEY')
        response = openai.Completion.create(
            model="text-davinci-003",
            prompt=input,
            temperature=0,
            max_tokens=1000,
            top_p=1,
            frequency_penalty=0,
            presence_penalty=0,
            stop=[" Human:", " AI:"]
        )
        ai_return = response.choices[0].text.strip()
        chat_history += 'AI:{}\n'.format(ai_return)
        with open('users_chat_history/{}_history.txt'.format(update.message.from_user.id), 'w+') as f:
            f.write(chat_history)
        await update.message.reply_text(ai_return, parse_mode=ParseMode.MARKDOWN)


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    message = "您好 我是ChatGPT 機器人 歡迎使用此bot \n若要使用ChatGPT 回應的功能請將要輸入的內容前面加上input:\n例如: input 簡單介紹CHATGPT是什麼\n更多功能請輸入指令/help查看"
    await update.message.reply_text(message, parse_mode=ParseMode.MARKDOWN)


async def help(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    message = "/start 啟用機器人\n /delete 刪除使用者存在服務器的聊天室內容\n"
    await update.message.reply_text(message, parse_mode=ParseMode.MARKDOWN)


async def delete(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    if os.path.exists('users_chat_history/{}_history.txt'.format(update.message.from_user.id)):
        os.remove('users_chat_history/{}_history.txt'.format(update.message.from_user.id))
        message = "chat id {} 聊天記錄已刪除".format(update.message.from_user.id)
        await update.message.reply_text(message, parse_mode=ParseMode.MARKDOWN)
    else:
        message = "chat id {} 聊天記錄不存在".format(update.message.from_user.id)
        await update.message.reply_text(message, parse_mode=ParseMode.MARKDOWN)


def main():
    try:
        TOKEN = read_config('SYSTEM', 'BOT_KEY')
        application = Application.builder().token(TOKEN).build()
        application.add_handler(MessageHandler(filters.ALL, echo), -1)
        application.add_handler(CommandHandler('start', start))
        application.add_handler(CommandHandler('help', help))
        application.add_handler(CommandHandler('delete', delete))
        application.run_polling()
    except Exception as e:
        logger.error(e)


if __name__ == '__main__':
    main()
